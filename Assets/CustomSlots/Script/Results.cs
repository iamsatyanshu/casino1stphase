﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ResultsData", menuName = "Result Data", order = 51)]


    public class Results : ScriptableObject
    {
        [SerializeField]
        private string swordName;
        [SerializeField]
        private string description;
        [SerializeField]
        private Sprite icon;
        [SerializeField]
        private int goldCost;
        [SerializeField]
        private int attackDamage;
    }

