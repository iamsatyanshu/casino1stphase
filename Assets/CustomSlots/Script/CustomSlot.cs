﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace CSFramework
{
    /// <summary>
    /// The main class of the CustomSlots Framework.
    /// </summary>
    public class CustomSlot : MonoBehaviour
    {
        public enum State
        {
            NotStarted,
            Idle,
            SpinStarting,
            Spinning,
            SpinStopping,
            Result,
        }

        [Serializable]
        public class Callbacks
        {
            public UnityEvent onActivated;
            public UnityEvent onDeactivated;
            public UnityEvent onRoundStart;
            public ReelInfo onReelStart;
            [Hide] public UnityEvent onNewSymbolAppear;
            public ReelInfo onReelStop;
            /* [Hide]*/
            public UnityEvent onRoundInterval;
            public HitInfo onProcessHit;
            public UnityEvent onRoundComplete;
            [Hide] public UnityEvent onSlotStateChange;
            public SlotModeInfo onSlotModeChange;
            [Hide] public BalanceInfo onAddBalance;
            public LineInfo onLineSwitch;

        }

        [Serializable]
        public class ForcedOutcomes
        {
            public int rowIndex;
            public Symbol[] forcedSymbols = new Symbol[5];

            public int RowIndex { get => rowIndex; set => rowIndex = value; }
            public Symbol[] ForcedSymbols { get => forcedSymbols; set => forcedSymbols = value; }
        }

        // public static CustomSlot InstanceCust { get; private set; }
        public SkinManager skin;
        public LineManager lineManager;
        public SymbolManager symbolManager;
        [Space]
        [Header("FORCED RESULTS")]
        public bool forceResults = false;
        public ForcedOutcomes[] forcedResult;

        [Space] public Button tripleBetButton;
        [Space] public TextMeshProUGUI tripleBetAmount;
        [Space] public TextMeshProUGUI tripleBetText;
        public bool StopThisSlot = false;
        [Header("Win Chain Components")]
        public RectTransform winPanel;
        public List<int> ChainAmountList;
        //public Image winSymbolImage;
        //public TextMeshProUGUI winChainCount;
        public TextMeshProUGUI winLineNumber;
        public TextMeshProUGUI winChainAmount;
        //Sprite winSymbolSprite;
        Color winBoxColor;
        public Image[] winSymbolList;
        public Sprite[] winSymbolSprite;
        public GameObject trippleText;
        public GameObject symbolListBox;
        public TextMeshProUGUI paysText;

        private Sequence sequence;
        private Sequence sequence2;
        List<SymbolHolder> list;
        [Hide] public SymbolGen symbolGen;
        [Hide] public GridLayoutGroup layoutReel;
        [Hide] public GridLayoutGroup layoutRow;
        [Hide] public RectTransform mainScreen;
        //[Space] public List<ForcedOutcomes> forcedMechanics;
        [Space] public SlotConfig config;
        [Space] public SlotModeManager modes;
        [Space] public SlotEffectManager effects;
        [Space] public Callbacks callbacks;
        [Space] public SlotLayouter layout;
        [Space] public SlotDebug debug;

        // [HideInInspector]
        public Reel[] reels;
        // [HideInInspector]
        public Row[] rows;

        private bool isInitialized;
        private int currentReelIndex = 0;
        private Sequence sequenceResult;
        private Queue<SlotEvent> events = new Queue<SlotEvent>();
        [SerializeField]
        private TextMeshProUGUI winAmount;
        private float payoutAmount;
        private bool isDollarFound = false;
        public int dollarCounts = 0;
        private int i;
        bool ScatterHits;


        public GameInfo gameInfo { get; private set; }
        public State state { get; private set; }
        public SlotEvent currentEvent { get; private set; }
        public SlotMode currentMode { get { return modes.current; } }
        public bool isIdle { get { return !isLocked && state == State.Idle; } }
        public bool isLocked { get { return currentEvent != null || events.Count > 0; } }

        public TextMeshProUGUI WinAmount { get => winAmount; set => winAmount = value; }
        public bool IsTripleSlot { get; internal set; }
        public float PayoutAmount { get => payoutAmount; set => payoutAmount = value; }
        public Sequence Sequence { get => sequence; set => sequence = value; }
        public Sequence Sequence2 { get => sequence2; set => sequence2 = value; }
        public bool IsDollarFound { get => isDollarFound; set => isDollarFound = value; }
        public int DollarCounts { get => dollarCounts; set => dollarCounts = value; }
      

        private void Awake()
        {
            
            //   InstanceCust = this;
            if (config.autoActivate) Activate();
            ResetWinTripleTextBox();

        }

        public void Start()
        {
            winSymbolSprite = new Sprite[winSymbolList.Length];
            //winSymbolSprite = winSymbolImage.sprite;
            winPanel.gameObject.SetActive(false);
            var winImagebg = winPanel.GetComponent<Image>();
            winBoxColor = winImagebg.color;
            for (int i = 0; i < winSymbolSprite.Length; i++)
            {
                winSymbolSprite[i] = winSymbolList[i].sprite;
            }

        }
        public void Initialize()
        {
            if (isInitialized) return;
            isInitialized = true;
            gameInfo = new GameInfo(this);
            if (!config.advanced.skipStartupValidation) Validate();
            modes.Initialize();
            layout.SetActiveLayout(false);
            lineManager.SwitchAllLines(false, true);
        }

        public void Activate()
        {
            Initialize();
            Time.timeScale = 1f;
            gameObject.SetActive(true);
            effects.transitionIn.Play(this, false, _Activate);
        }

        private void _Activate()
        {
            if (!debug.skipIntro) effects.introAnimation.Play(this);
            if (state == State.NotStarted && config.autoStartRound) AddEvent(StartRound);
            callbacks.onActivated.Invoke();
        }

        public void Deactivate(bool destroy = false) { effects.transitionOut.Play(this, true, () => { _Deactivate(destroy); }); }

        private void _Deactivate(bool destroy)
        {
            callbacks.onDeactivated.Invoke();
            if (destroy) Destroy(gameObject);
            else gameObject.SetActive(false);
        }

        /// <summary>
        /// Validate() should be called when a slot needs to refresh its data and layout.
        /// (Number of reels and rows, adding/removing symbols and lines etc)
        /// </summary>
        public void Validate()
        {
            symbolManager.Validate(this);
            lineManager.Validate(this);
            reels = layoutReel.transform.GetComponentsInChildren<Reel>();
            rows = layoutRow.transform.GetComponentsInChildren<Row>();
            foreach (Reel reel in reels) reel.Validate(this);

        }

        /// <summary>
        /// Queue an event to CustomSlot's event system.
        /// </summary>
        public SlotEvent AddEvent(SlotEvent e)
        {
            events.Enqueue(e);
            return e;
        }

        public SlotEvent AddEvent(Sequence sequence) { return AddEvent(new EventTweenSequence(sequence)); }
        public SlotEvent AddEvent(float duration, TweenCallback onStart = null, TweenCallback onComplete = null) { return AddEvent(Util.Sequence(duration, onStart, onComplete)); }
        public SlotEvent AddEvent(TweenCallback action) { return AddEvent(Util.Sequence(0, action)); }
        public SlotEvent AddEvent(Tweener tween) { return AddEvent(DOTween.Sequence().Join(tween)); }

        public void SwitchState(State newState)
        {
            state = newState;
            callbacks.onSlotStateChange.Invoke();
        }

        public void SwitchMode() { if (state == State.Idle) modes.SwitchMode(); }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                layout.sizeSymbol = new Vector2(100, 100);
                layout.Refresh(false);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                layout.sizeSymbol = new Vector2(150, 150);

                layout.Refresh(false);
            }

            // If there's an active event, skip all other updates until the event is finished.
            if (currentEvent != null)
            {
                currentEvent.Update();
                if (currentEvent.isDeactivated) currentEvent = null;
                else return;
            }

            if (events.Count > 0)
            {
                currentEvent = events.Dequeue();
                currentEvent.Activate();
                return;
            }

            switch (state)
            {
                case State.Idle:
                    if (currentMode.forcePlay) StartSpin();
                    debug.OnUpdate();
                    break;

                case State.SpinStarting:

                    break;

                case State.Spinning:

                    break;

                case State.SpinStopping:
                    for (int i = 0; i < reels.Length; i++)
                        if (reels[i].isSpinning) return;
                    callbacks.onRoundInterval.Invoke();

                    SwitchState(State.Result);

                    break;

                case State.Result:
                    foreach (Line line in lineManager.lines)
                        if (line.hitInfo.ProcessHitCheck()) return;
                    if(gameInfo.scatterHitInfos != null)
                    {
                        foreach (HitInfo hitInfo in gameInfo.scatterHitInfos)
                            if (!hitInfo.hitSymbol.ignoreThisRound && hitInfo.ProcessHitCheck()) return;
                    }
                   
                    gameInfo.OnRoundComplete();
                    ClearManipulation();
                    SwitchState(State.NotStarted);

                    callbacks.onRoundComplete.Invoke();
                    if (effects.lineHitEffect.displayAsPlayback)
                    {

                        StartCoroutine("ABC");


                    }



                    if (config.autoStartRound) StartRound();
                    break;
            }

            if (IsTripleSlot)
            {
                ShowTripleWinText();
            }
            else
            {
                ResetWinTripleTextBox();
            }

            // winAmount.text = BaseSlotGameUI.Instance.Balance.ToString();

        }



        public void StartRound()
        {
            // BaseSlotGameUI.Instance.checkbalance();
            if (forceResults)
            {
                ForcedResults();
            }
            else
            {
                gameInfo.OnStartRound();
            }
            
            SwitchState(State.Idle);
            SwitchMode();
            symbolManager.OnStartRound();
            lineManager.OnStartRound();
            callbacks.onRoundStart.Invoke();
            if (currentMode.forcePlay) Play();
        }

        public void ShowTripleWinText()
        {
            int i = 1;
            trippleText.gameObject.SetActive(true);
            trippleText.gameObject.transform.SetSiblingIndex(0);
            symbolListBox.gameObject.transform.SetSiblingIndex(i);
            paysText.transform.SetSiblingIndex(i + 1);
            winChainAmount.transform.SetSiblingIndex(i + 2);



        }

        public void ResetWinTripleTextBox()
        {
            int i = 0;
            trippleText.gameObject.SetActive(false);
            // trippleText.gameObject.transform.SetSiblingIndex(1);
            symbolListBox.gameObject.transform.SetSiblingIndex(i);
            paysText.transform.SetSiblingIndex(i + 1);
            winChainAmount.transform.SetSiblingIndex(i + 2);
        }
        public void Play()
        {
            ChainAmountList.Clear();
            IsDollarFound = false;
            dollarCounts = 0;
            ResetWinBox();
            effects.lineHitEffect.displayAsPlayback = true;
            //BaseSlotGameUI.Instance.PlayButton.interactable = false;

            if (isLocked) return;
            switch (state)
            {
                case State.Idle:
                    StartSpin();
                    break;

                case State.Spinning:
                    if (currentMode.spinMode == SlotMode.SpinMode.ManualStopAll)
                        StopSpin();
                    if (currentMode.spinMode == SlotMode.SpinMode.ManualStopOne) StopReel();
                    if (currentMode.spinMode == SlotMode.SpinMode.ManualStartOne)
                    {
                        StopReel();
                        StartReel();
                    }
                    break;
            }
        }

        /// <summary>
        /// Starts spinning all the reels in a sequence and changes the State to "Intro".
        /// </summary>
        public void StartSpin(float duration = 0)
        {

            payoutAmount = 0;
            if (!isIdle) return;
            StopPlaybackResult();

            SwitchState(State.SpinStarting);
            if (duration == 0)
            {



                duration = currentMode.spinMode == SlotMode.SpinMode.AutoStop ? currentMode.autoStopTime + 0.1f : 0;





            }

            float _delay = debug.fastSpin ? 0 : currentMode.spinStartDelay;
            if (debug.alwaysMaxLines) lineManager.SwitchAllLines(true, true);
            currentReelIndex = 0;

            StopCoroutine("StopSpinCoroutine");

            if (currentMode.spinMode == SlotMode.SpinMode.ManualStartOne)
            {
                StartReel();
            }
            else
            {
                Sequence sequence = DOTween.Sequence();
                foreach (Reel reel in reels)
                    sequence.Append(Util.Tween(_delay, reel.Spin));
                sequence.Append(currentMode.reelAccelerateTime, OnIntroComplete);
                AddEvent(sequence);
            }
            gameInfo.OnStartSpin();

            if (duration > 0)// && StopThisSlot)
            {



                AddEvent(duration, null, StopSpin);
                // AddEvent(duration, null, StopSpin);



            }
        }

        public void StartReel()
        {
            if (currentReelIndex >= reels.Length) return;
            Sequence sequence = DOTween.Sequence();
            sequence.Append(Util.Tween(currentMode.spinStartDelay, reels[currentReelIndex].Spin));
            sequence.Append(currentMode.reelAccelerateTime, OnIntroComplete);
            AddEvent(sequence);
        }

        /// <summary>
        /// Stops a spinning reel . If the reel is the last spinning reel, CustomSlot will start performing Hit Check.
        /// </summary>
        public void StopReel()
        {

            reels[currentReelIndex].Stop();
            currentReelIndex++;
            if (currentReelIndex == 5)
            {
                // Testing(currentReelIndex);
            }

            if (currentReelIndex >= reels.Length) SwitchState(State.SpinStopping);
        }

        /// <summary>
        /// Stops all the spinning reels and changes the State to "Outro". When all the reels stop animating, Hit Check will be performed on each line.
        /// </summary>
        public void StopSpin()
        {

            if (this.name == "Custom Slot 1")
            {

                SwitchState(State.SpinStopping);
                StartCoroutine("StopSpinCoroutine");
            }


            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    Debug.Log("PRESSING L");
            //    SwitchState(State.SpinStopping);
            //    StartCoroutine("StopSpinCoroutine");
            //}

            //BaseSlotGameUI.Instance.CheckSlotStopped(this);
            //if (!StopThisSlot)
            //{
            //    return;
            //}
            //else 
            //{
            //    SwitchState(State.SpinStopping);            ////Main
            //    StartCoroutine("StopSpinCoroutine");
            //}





        }


        public void TestingSpinState(CustomSlot activeSlot)
        {
            // Debug.Log(text_);
            if (this.name == "Custom Slot 1")
            {
                //activeSlot.StopThisSlot = true;
                Debug.Log("STOPPED SLOT :: " + this.name);
                this.gameInfo.slotStopped = true;

            }

            else
                for (int i = 0; i < BaseSlotGameUI.Instance.slots.Length; i++)
                {
                    if (this == BaseSlotGameUI.Instance.slots[i].GetComponent<CustomSlot>())
                    {
                        //if (i == 0)
                        //{
                        //    activeSlot.StopThisSlot = true;
                        //    CustomSlot xxxx = slots[i + 2].GetComponent<CustomSlot>();
                        //    xxxx.StopThisSlot = true;
                        //}
                        //else
                        //{
                        CustomSlot xx = BaseSlotGameUI.Instance.slots[i - 1].GetComponent<CustomSlot>();
                        if (xx.gameInfo.slotStopped)
                        {
                            Debug.Log("STOPPED SLOT :: " + xx + "   TRY STOPPING :: " + activeSlot);
                            if (i < BaseSlotGameUI.Instance.slots.Length)
                            {
                                CustomSlot xxxx = BaseSlotGameUI.Instance.slots[i + 1].GetComponent<CustomSlot>();
                                xxxx.StopThisSlot = true;
                            }

                        }

                    }
                }
        }

        public void SpicMech(CustomSlot activeSlot)
        {
            activeSlot.SwitchState(State.SpinStopping);
            activeSlot.StartCoroutine("StopSpinCoroutine");
            if (this.name == "Custom Slot 4")
            {

                BaseSlotGameUI.Instance.StoppedAll = true;
            }

        }

        public IEnumerator StopSpinCoroutine()
        {
            float delay = debug.fastSpin ? 0 : currentMode.spinStopDelay;


            while (currentReelIndex < reels.Length)
            {

                if (currentReelIndex == 4)
                {
                    //yield return new WaitForSeconds(2f);
                    StopReel();
                }
                else
                {
                    StopReel();
                }

                while (reels[currentReelIndex - 1].isSearchingForSymbol) { yield return null; }
                yield return new WaitForSeconds(delay);
            }

        }

        private void OnIntroComplete() { SwitchState(State.Spinning); }

        public IEnumerator ABC()
        {

           // yield return new WaitUntil(() => BaseSlotGameUI.Instance.StoppedAll == true);
            StartPlaybackResult();
            yield return null;
        }

        internal void ProcessHit(HitInfo info)
        {


            info.payout = info.hitSymbol.GetPayAmount(info.hitChains);
            ChainAmountList.Add(info.payout);

            gameInfo.AddHit();

            if (info.hitSymbol.payType == Symbol.PayType.Normal)
            {
                if (this.IsTripleSlot == true)
                {
                    gameInfo.AddBalance(info.payout * (gameInfo.bet*3), info);

                    payoutAmount += info.payout * (gameInfo.bet*3);
                    BaseSlotGameUI.Instance.assets.audioWinBig.Play();
                }
                else
                {
                    gameInfo.AddBalance(info.payout * gameInfo.bet, info);

                    payoutAmount += info.payout * gameInfo.bet;
                    BaseSlotGameUI.Instance.assets.audioWinMedium.Play();
                }
                
            }
            else if (info.hitSymbol.payType == Symbol.PayType.FreesSpin)
            {
                //IsDollarFound = true;
                // BaseSlotGameUI.Instance.DollarCounts++;

                //BaseSlotGameUI.Instance.AllFreeCounts += DollarCounts;
                //if (BaseSlotGameUI.Instance.AllFreeCounts <= 2 && BaseSlotGameUI.Instance.AllFreeCounts != 0)
                //{
                //    AddFreeSpin(8);
                //    Debug.Log("FREE SPINS :: " + BaseSlotGameUI.Instance.AllFreeCounts);
                //}
                //if (BaseSlotGameUI.Instance.AllFreeCounts > 2)
                //{
                //    AddFreeSpin(15);
                //    Debug.Log("FREE SPINS :: " + BaseSlotGameUI.Instance.AllFreeCounts);
                //}
                // AddFreeSpin(5);

                Debug.Log("DollarCounts " + BaseSlotGameUI.Instance.DollarCounts);
                ScatterHits = true;
            }
            else if (info.hitSymbol.payType == Symbol.PayType.Bonus)
            {
                AddBonus(info.payout);
            }
            else if (info.hitSymbol.payType == Symbol.PayType.Wild2x)
            {
                AddBonus(info.payout);
                BaseSlotGameUI.Instance.assets.audioWinBig.Play();
            }
            else if (info.hitSymbol.payType == Symbol.PayType.Wild3x)
            {
                AddBonus(info.payout);
                BaseSlotGameUI.Instance.assets.audioWinBig.Play();
            }

            if (!effects.lineHitEffect.displayAsPlayback)
            {
                AddEvent(DisplayHitEffect2(info));
            }



        }
        public Sequence DisplayHitEffect(HitInfo info)
        {



            sequence = info.sequence = DOTween.Sequence();

            SlotEffectManager.SymbolHitEffect hitEffect = effects.GetHitEffect(info);


            for (int i = 0; i < info.hitHolders.Count; i++)
            {

                info.hitHolders[i].HighlightBorder(2f).SetLoops(1, LoopType.Yoyo);

            }
            // StartCoroutine(Test(info));





            return sequence;
        }


        IEnumerator Test(HitInfo info)
        {
            yield return new WaitForSeconds(1f);
            AddEvent(DisplayHitEffect2(info));

        }

        void Test1(HitInfo info)
        {
            //StartCoroutine(Test(info));
            AddEvent(DisplayHitEffect2(info));
        }

        public Sequence DisplayHitEffect2(HitInfo info)
        {


            Debug.Log("GETTING HIT");
            if (info != null)



                Sequence2 = info.sequence = DOTween.Sequence();
            Sequence2.OnStart(() =>
            {
                if (!info.isSequencePlayed)
                {
                    callbacks.onProcessHit.Invoke(info);
                    info.isSequencePlayed = true;
                }
            });
            SlotEffectManager.SymbolHitEffect hitEffect = effects.GetHitEffect(info);
            if (hitEffect == null)
            {
                Sequence2.Append(Util.Tween(1f));
            }
            else
            {
                for (int i = 0; i < info.holders.Length; i++)

                    if (i < info.hitChains)
                    {

                        Sequence2.Join(hitEffect.Play(info.holders[i].link, i, info));


                    }

                if (info.line)
                    Sequence2.Join(effects.lineHitEffect.Play(info.line, hitEffect.duration));





            }
            return Sequence2;
        }

        public void ShowPayoutBox(HitInfo info)
        {
            winPanel.gameObject.SetActive(true);
            if (IsTripleSlot)
            {
                winChainAmount.text = "" + ((info.payout) * 3);
            }
            else
                winChainAmount.text = "" + info.payout;
            // ResetWinBox();
            for (int i = 0; i < winSymbolList.Length; i++)
            {
                winSymbolList[i].sprite = info.hitHolders[i].symbol.sprite;
            }
            // var winImagebg = winPanel.GetComponent<Image>();
            // winImagebg.color = info.line.hitInfo.line.gradientColor;
            //  winChainCount.text = "" + info.hitChains;
            winLineNumber.text = "Win On Line " + info.line.hitInfo.line.order;
            winChainAmount.text = "" + info.payout;

            // winSymbolImage.sprite = info.hitSymbol.sprite;
            // ResetWinBox();


            // ResetWinBox();
            for (int i = 0; i < winSymbolList.Length; i++)
            {
                // winSymbolList[i].sprite = info.holders[i].symbol.sprite;
                winSymbolList[i].sprite = info.hitHolders[i].symbol.sprite;
            }
            // var winImagebg = winPanel.GetComponent<Image>();
            // winImagebg.color = info.line.hitInfo.line.gradientColor;
            //  winChainCount.text = "" + info.hitChains;

        }

        public void ResetWinBox()
        {
            winPanel.gameObject.SetActive(false);
            //  var winImagebg = winPanel.GetComponent<Image>();
            // winImagebg.color = winBoxColor;
            // winChainCount.text = "";
            winLineNumber.text = "Win On Line ";
            winChainAmount.text = "";
            for (int i = 0; i < winSymbolList.Length; i++)
            {
                winSymbolList[i].sprite = winSymbolSprite[i];
            }
            //winSymbolImage.sprite = winSymbolSprite;
        }

        private void StartPlaybackResult()
        {
            //int x = 0;

            //HitInfo firstHit = (from line in lineManager.lines where line.hitInfo.isHit select line.hitInfo).FirstOrDefault();
            //if (firstHit == null)
            //    foreach (HitInfo hitInfo in gameInfo.scatterHitInfos)
            //        if (!hitInfo.hitSymbol.ignoreThisRound && hitInfo.isHit) firstHit = hitInfo;
            //if (firstHit == null)
            //    return;
            //sequenceResult = DisplayHitEffect2(firstHit);
            ////sequenceResult.WaitForCompletion();

            //for (i = 0; i < lineManager.lines.Length; i++)
            //{
            //    if (lineManager.lines[i].hitInfo.isHit)
            //        x++;

            //}

            //foreach (Line line in lineManager.lines)
            //{


            //    if (x < 2)
            //    {


            //        if (line.hitInfo.isHit && line.hitInfo != firstHit)
            //        {


            //            sequenceResult.Append(DisplayHitEffect(line.hitInfo)).Append(DisplayHitEffect2(line.hitInfo)).SetDelay(1f);

            //        }

            //    }
            //    else
            //    {

            //        if (line.hitInfo.isHit && line.hitInfo != firstHit)
            //        {


            //            sequenceResult.Append(DisplayHitEffect(line.hitInfo)).Append(DisplayHitEffect2(line.hitInfo)).SetDelay(1f);

            //        }
            //    }


            //}

            //foreach (HitInfo hitInfo in gameInfo.scatterHitInfos)
            //    if (!hitInfo.hitSymbol.ignoreThisRound && hitInfo.isHit && hitInfo != firstHit)
            //        sequenceResult.Append(DisplayHitEffect(hitInfo));
            //HitInfo info = (from line in lineManager.lines where line.hitInfo.isHit select line.hitInfo).Single();
            //sequence = info.sequence = DOTween.Sequence();

            //SlotEffectManager.SymbolHitEffect hitEffect = effects.GetHitEffect(info);


            //for (int i = 0; i < info.hitHolders.Count; i++)
            //{

            //    info.hitHolders[i].HighlightBorder(2f).SetLoops(1, LoopType.Yoyo);

            //}
            //    if (!BaseSlotGameUI.Instance.StoppedAll)
            //{
            //    return;
            //}
            //    else

            {
                HitInfo firstHit = (from line in lineManager.lines where line.hitInfo.isHit select line.hitInfo).FirstOrDefault();
                if (firstHit == null) 
                    if(gameInfo.scatterHitInfos != null)
                    foreach (HitInfo hitInfo in gameInfo.scatterHitInfos)
                        if (!hitInfo.hitSymbol.ignoreThisRound && hitInfo.isHit) firstHit = hitInfo;
                if (firstHit == null) return;
                sequenceResult = DisplayHitEffect2(firstHit);
                foreach (Line line in lineManager.lines)
                    if (line.hitInfo.isHit && line.hitInfo != firstHit)
                        sequenceResult.Append(DisplayHitEffect2(line.hitInfo));
                if(gameInfo.scatterHitInfos != null)
                foreach (HitInfo hitInfo in gameInfo.scatterHitInfos)
                    if (!hitInfo.hitSymbol.ignoreThisRound && hitInfo.isHit && hitInfo != firstHit)
                        sequenceResult.Append(DisplayHitEffect2(hitInfo));
            }

        }

        public void StopPlaybackResult()
        {
            if (sequenceResult != null)
            {
                sequenceResult.Complete(true);

                sequenceResult = null;



            }
            if (sequence != null)
            {
                sequence.Complete(true);

                sequence = null;



            }
            if (sequence2 != null)
            {
                sequence2.Complete(true);

                sequence2 = null;
            }
            sequence.Kill();
            sequence2.Kill();
            sequenceResult.Kill();
            BaseSlotGameUI.Instance.temp = 4;
        }

        /// <summary>
        /// Returns a list of all visible SymbolHolder.  
        /// </summary>
        public List<SymbolHolder> GetVisibleHolders()
        {
            List<SymbolHolder> list = new List<SymbolHolder>();
            foreach (Row row in rows)
            {
                if (row.isHiddenRow) continue;
                foreach (SymbolHolder holder in row.holders)
                {
                    list.Add(holder);

                }


            }
            return list;
        }

        public void Testing(int reelnumber)
        {

            Debug.Log("STOPPING SLOT ::  " + this.name /*+ "   Reel No ::  " + reelnumber*/);


        }

        public List<SymbolHolder> GetAllHolders()
        {
            List<SymbolHolder> list = new List<SymbolHolder>();
            foreach (Reel reel in reels)
                foreach (SymbolHolder holder in reel.holders)
                {
                    list.Add(holder);
                    Debug.Log(holder.symbol.name);
                }
            return list;
        }

        public Reel GetReelAt(int index) { return index < 0 || index >= reels.Length ? null : reels[index]; }

        //public void AddFreeSpin(int amount)
        //{
        //    foreach (CustomSlot slot in BaseSlotGameUI.Instance.slots)
        //    {
        //        slot.gameInfo.freeSpins += amount;
        //        if (slot.gameInfo.freeSpins < 0)
        //            slot.gameInfo.freeSpins = 0;
        //    }

        //}

        public void AddFreeSpinsAll()
        {
            //if (BaseSlotGameUI.Instance.AllFreeCounts <= 2)
            //{
            //    foreach (CustomSlot slot in BaseSlotGameUI.Instance.slots)
            //    {
            //        slot.gameInfo.freeSpins += 8;
            //        if (slot.gameInfo.freeSpins < 0)
            //            gameInfo.freeSpins = 0;
            //        Debug.Log("TOTALL NUMBER FO FREE SPINS ::   " + slot.gameInfo.freeSpins);
            //    }

            //}
            //else
            //{
            //    foreach (CustomSlot slot in BaseSlotGameUI.Instance.slots)
            //    {
            //        slot.gameInfo.freeSpins += 15;
            //        if (slot.gameInfo.freeSpins < 0)
            //            slot.gameInfo.freeSpins = 0;
            //        Debug.Log("TOTALL NUMBER FO FREE SPINS ::   " + slot.gameInfo.freeSpins);
            //    }

            //}
        }

        public void AddBonus(int amount)
        {
            gameInfo.bonuses += amount;
            if (gameInfo.bonuses < 0) gameInfo.bonuses = 0;
        }

        public void SetBet(int amount) { gameInfo.bet = amount; }

        public void Spinner()
        {

            BaseSlotGameUI.Instance.temp++;

        }

        /// <summary>
        /// Set the result for the next spin. 
        /// The first parameter(rowOffset) is an offset from the top visible row. When specified 0 the symbols will land on the top visible row, 1 for the 2nd row and etc....
        /// Specify symbol(s) you want to land in the result in the second parameter.
        /// </summary>
        public void SetManipulation(int rowOffset, string symbolId)
        {
            Symbol symbol = symbolManager.GetSymbol(symbolId);
            if (symbol) foreach (Reel reel in reels) reel.SetManipulation(symbol, 0, rowOffset);
        }

        public void SetManipulation(int rowOffset, Symbol symbol)
        {
            foreach (Reel reel in reels) reel.SetManipulation(symbol, 0, rowOffset);
        }
        public void SetManipulation(int rowOffset, params Symbol[] symbols)
        {
            for (int i = 0; i < symbols.Length; i++) if (i < reels.Length)
                    reels[i].SetManipulation(symbols[i], 2, rowOffset);
        }
        public void SetManipulation(int rowOffset, params int[] symbolIndexes)
        {
            for (int i = 0; i < symbolIndexes.Length; i++) if (i < reels.Length)
                    reels[i].SetManipulation(null, symbolIndexes[i], rowOffset);
        }
        public void ForcedResults()
        {
            //foreach(ForcedOutcomes Outcome in forcedResult)
            //{
            //    SetManipulation(Outcome.RowIndex, Outcome.ForcedSymbols[0], Outcome.ForcedSymbols[1], Outcome.ForcedSymbols[2], Outcome.ForcedSymbols[3], Outcome.ForcedSymbols[4]);
            //}
         for(int i=0;i < forcedResult.Length; i++)
            {
                SetManipulation(forcedResult[i].RowIndex, forcedResult[i].ForcedSymbols[0], forcedResult[i].ForcedSymbols[1], forcedResult[i].ForcedSymbols[2], forcedResult[i].ForcedSymbols[3], forcedResult[i].ForcedSymbols[4]);
            }
                
        }
        private void ClearManipulation()
        {
            foreach (Reel reel in reels) reel.ClearManipulation();
        }
    }
}
