﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections;
using UnityEditor;
using DG.Tweening;


namespace CSFramework
{
    /// <summary>
    /// A base UI class you can use for your slot game.
    /// It is not necessary to inherit this class
    /// </summary>
    public class BaseSlotGameUI : MonoBehaviour
    {

        public static BaseSlotGameUI Instance { get; private set; }
        public float Balance { get => balance; set => balance = value; }
        public int AllFreeCounts { get => allFreeCounts; set => allFreeCounts = value; }
        public float Cduration = 0;
        [Serializable]
        public class Assets
        {
            //[Serializable]
            //public class Tweens
            //{
            //    public TweenSprite tsBonus, tsIntro1, tsIntro2, tsWin, tsWinSpecial;
            //}

            //public Tweens tweens;

            public AudioSource bgm, audioEarnSmall, audioEarnBig, audioPay, audioSpin, audioSpinLoop, audioReelStop, audioClick;
            public AudioSource audioWinSmall, audioWinMedium, audioWinBig, audioLose, audioBet, audioImpact, audioBeep;
            public AudioSource audioBonus, audioWinSpecial, audioSpinBonus;
            //public ParticleSystem particlePay, particlePrize, particleFreeSpin;
            //public ElosEffectMoney effectMoney;
            //public ElosEffectBalloon effectBalloon;
        }

        public Assets assets;

        private float balance = 6000;
        public CustomSlot[] slots;
        public float timeBetweenSpins;
        public bool[] isRichSlot = new bool[] { false, false, false, false };
        public int[] FreeSpin = new int[] { 0, 0, 0, 0 };
        public Text textMoney, textRoundCost, textRound, textIncome, textBet, textFreeSpin, textBonus, debugText;
        public TextMeshProUGUI textMoney_, textBet_, textRoundCost_, textIncome_, marketingtxt;
        public GameObject goFreeSpin, goBonus;
        public List<int> betList = new List<int>() { 1, 2, 3, 4, 5 };
        //SCREEN VARIABLES
        public GameObject startscreen;
        public GameObject screenfooter;

        public GameObject screenheader;
        public GameObject titleheader;
        public GameObject Image_PlayTable;
        public GameObject Title_PlayTable;
        public GameObject paybacklogo;
        public bool BonusAnimComplete = false;
        private int allFreeCounts = 0;
        public bool StoppedAll = false;

        public GameObject marketingText;
        //public GameObject screenheader;
        public GameObject panel1;
        // public GameObject panel3;
        public GameObject panel4;// Added by Pragya
        public GameObject playtable;
        public GameObject[] playtables;// Added by Pragya
        public GameObject NextBtn; // Added by Pragya
        public GameObject BackBtn; //Added by Pragya
        public Button PlayButton;
        public Sprite freeSpinButton;
        public Button LineAdd;//Added by Pragya
        public Button LineRemove;//Added by Pragya
        public Button BetDecrease;//Added by Pragya
        public Button BetIncrease;//Added by Pragya
        int mybetCost = 0;          //Satyanshu
        int myRoundCost = 0;
        int myLineCost = 0;
        public int targetFrameRate = 70;
        private int betIndex = 1;
        public GameObject navbar;
        private bool stopped;
        public int temp = 0;
        public int compare = 0;
        public string[] marketingTxtChange = new string[5];
        public GameObject[] MarketingIconList;
        public Image MarketingIcon;
        public Image MarketingIcon2;
        public Image MarketingIcon3;
        [Hide] public bool ShowMessage;
        public float MarketingTxtTime;
        private IEnumerator coroutine;

        float roundCost;
        public int RQLC = 0;
        private Image defaultPlayImage;
        float xx;
        public GameObject BonusBG;
        public GameObject playScreens;
        public GameObject[] FooterPanel;
        public GameObject SpinButton;
        public Button InfoButton;
        public GameObject StartButton;
        public GameObject BonusMessage;
        public int CountScatter;
        public int dollarCounts = 0;
        public int TotalFreeSpin = 0;
        public Button Sound;

        public Sprite mute;
        public Sprite unmute;

        public int DollarCounts { get => dollarCounts; set => dollarCounts = value; }
        // public Button[] TripleButtons;
        protected virtual void Awake()
        {

            if (Instance == null)
            {
                Instance = this;
            }

            foreach (CustomSlot slot in slots)
            {
                slot.callbacks.onActivated.AddListener(OnActivated);
                slot.callbacks.onDeactivated.AddListener(OnDeactivated);
                slot.callbacks.onRoundStart.AddListener(OnRoundStart);
                slot.callbacks.onReelStart.AddListener(OnReelStart);
                slot.callbacks.onReelStop.AddListener(OnReelStop);
                slot.callbacks.onProcessHit.AddListener(OnProcessHit);
                slot.callbacks.onRoundComplete.AddListener(OnRoundComplete);
                slot.callbacks.onSlotStateChange.AddListener(OnSlotStateChange);
                slot.callbacks.onSlotModeChange.AddListener(OnSlotModeChange);
                slot.callbacks.onLineSwitch.AddListener(OnLineSwitch);
                slot.Activate();

            }

            Initialize();
        }
        private void Start()
        {
            assets.bgm.Play();
            dollarCounts = 0;
            Sound.interactable = false;
            Title_PlayTable.SetActive(false);
            Image_PlayTable.SetActive(false);
            titleheader.SetActive(false);
            paybacklogo.SetActive(false);
            startscreen.SetActive(true);
            screenfooter.SetActive(true);
            StartButton.SetActive(false);
            defaultPlayImage = PlayButton.targetGraphic.GetComponent<Image>();
            LineRemove.interactable = false;
            BetDecrease.interactable = true;
            marketingtxt = marketingText.gameObject.GetComponent<TextMeshProUGUI>();

            //SelectTripleSlotRandomly();

            foreach (CustomSlot slot in slots)
            {
                slot.lineManager.EnableAllLines();
            }
            MarketingIcon.enabled = false;
            MarketingIcon2.enabled = false;
            MarketingIcon3.enabled = false;
        }
        public void SetCopyrightText()
        {

        }
        public virtual void Initialize()
        {
            foreach (CustomSlot slot in slots)
            {
                Application.targetFrameRate = targetFrameRate;
                RefreshMoney();
                RefreshBet();
                RefreshRoundCost();
                goFreeSpin.SetActive(false);
                goBonus.SetActive(false);

                slot.SetBet(betList[betIndex]);
            }

        }


        public void PlayAllSlots()
        {
            xx = 0;
            //for (int i = 0; i < slots.Length; i++)
            //{
            //    if(slots[i].Sequence != null && slots[i].Sequence2 != null)
            //    if(slots[i].Sequence.IsActive() || slots[i].Sequence2.IsActive())
            //    {
            //            slots[i].DOKill();


            //    }

            //}

            StoppedAll = false;
            StartCoroutine(SpinSlots());

        }

        public IEnumerator SpinSlots()
        {
            InfoButton.interactable = false;
            PlayButton.interactable = false;
            BetIncrease.interactable = false;
            BetDecrease.interactable = false;
            //for (int i = 0; i < slots.Length; i++)
            //{
            //    slots[i].tripleBetButton.interactable = false;
            //   // TripleButtons[i].interactable = false;
            //}
            TripleButtonsEnable(false);

            for (int i = 0; i < slots.Length; i++)
            {

                slots[i].Play();

                yield return new WaitForSeconds(timeBetweenSpins);
            }
        }

        public void TripleButtonsEnable(bool enabler)
        {
            for (int i = 0; i < slots.Length; i++)
            {
                slots[i].tripleBetButton.interactable = enabler;
                // TripleButtons[i].interactable = false;
            }
        }
        /// <summary>
        /// A callback method subscribed to CS's onActivated event.
        /// It is invoked when CS is activated, precisely after an intro animation event(parallel progression).
        /// </summary>
        public virtual void OnActivated() { ShowDebugText("onActivated"); }

        /// <summary>
        /// A callback method subscribed to CS's onDeactivated event.
        /// It is invoked when CS is deactivated, after an Out-Transition is completed.
        /// </summary>
        public virtual void OnDeactivated() { ShowDebugText("onDeactivated"); }

        /// <summary>
        /// A callback method subscribed to CS's onRoundStart event.
        /// It is invoked at the start of every round.
        /// </summary>
        public virtual void OnRoundStart()
        {
            CountScatter = 0;

            ShowDebugText("onRoundStart");
            // Debug.Log("---------------------------------------");

            //foreach (CustomSlot slot in slots)
            //{
            //    if (slot.currentEvent == null)
            //        PlayButton.interactable = false;
            //}
            RefreshRoundCost();
            RefreshRoundInfo();

        }

        /// <summary>
        /// A callback method subscribed to CS's onStartSpin event.
        /// It is invoked each time a reel starts.
        /// </summary>
        public virtual void OnReelStart(ReelInfo info)
        {

            temp = 0;
            ShowDebugText("onStartSpin");
            if (info.isFirstReel)
            {
                assets.audioSpin.Play();
                assets.audioSpinLoop.Play();
                RefreshRoundCost();
                RefreshRoundInfo();
                RefreshMoney();
            }
        }

        public void CheckSlotStopped(CustomSlot activeSlot)
        {
            //if (activeSlot.name == "Custom Slot 1")
            //{
            //    //activeSlot.StopThisSlot = true;
            //    Debug.Log("STOPPED SLOT :: "+ activeSlot);

            //}

            //else
            for (int i = 0; i < slots.Length; i++)
            {
                if (activeSlot == slots[i].GetComponent<CustomSlot>())
                {
                    //if (i == 0)
                    //{
                    //    activeSlot.StopThisSlot = true;
                    //    CustomSlot xxxx = slots[i + 2].GetComponent<CustomSlot>();
                    //    xxxx.StopThisSlot = true;
                    //}
                    //else
                    //{
                    CustomSlot xx = slots[i - 1].GetComponent<CustomSlot>();
                    if (xx.gameInfo.slotStopped)
                    {
                        Debug.Log("STOPPED SLOT :: " + xx + "   TRY STOPPING :: " + activeSlot);
                        if (i < slots.Length)
                        {
                            CustomSlot xxxx = slots[i + 1].GetComponent<CustomSlot>();
                            xxxx.StopThisSlot = true;
                        }

                    }

                }
            }
        }

        public void TesterFunction()
        {
            Debug.Log("Tester Function is being hit in :: " + this);
        }
        private void OnGUI()
        {
            checkbalance();
            for (int i = 0; i < slots.Length; i++)
            {
                slots[i].tripleBetAmount.text = "" + slots[i].gameInfo.GetRounCost;
            }
            textMoney_.text = BaseSlotGameUI.Instance.Balance.ToString();

            //foreach (CustomSlot slot in slots)
            //{
            //    if (!slot.IsTripleSlot)
            //        slot.tripleBetAmount.text = "" + slot.gameInfo.GetRounCost;
            //    else
            //        slot.tripleBetAmount.text = "" + slot.gameInfo.GetRounCost;
            //}
            //if (Input.GetKeyDown(KeyCode.Space)== false)
            //{
            //    for (int i = 0; i < slots.Length; i++)
            //    {
            //        slots[i].tripleBetAmount.text = "" + slots[i].gameInfo.GetRounCost;
            //    }
            //}



        }

        /// <summary>
        /// A callback method subscribed to CS's onStopReel event.
        /// It is invoked each time a reel stops.
        /// ReelInfo contains information like which reel was stopped.
        /// </summary>
        public virtual void OnReelStop(ReelInfo info)
        {
            assets.audioReelStop.Play();
            if (info.isLastReel)
            {
                assets.audioSpinLoop.Stop();
            }

            // OnReelStop(info);
            //foreach (CustomSlot slot in slots)
            //{
            //    ShowDebugText("onStopReel");


            //    if (info.isFirstReel && slot.currentMode.spinMode == SlotMode.SpinMode.ManualStopAll) PlayButton.interactable = false;
            //    if (info.isLastReel)
            //    {
            //        PlayButton.interactable = false;
            //        //assets.audioSpinLoop.Stop();
            //    }
            //}
            //foreach (CustomSlot slot in slots)
            //{
            //    if (!slot.IsDollarFound)
            //    {
            //        return;


            //    }
            //    else
            //    {
            //        for (int i = 0; i < slots.Length; i++)
            //        {
            //            slots[i].AddFreeSpin(2);
            //        }
            //    }
            //}

        }

        /*
		/// <summary>
		/// A callback method subscribed to CS's onRoundInterval event.
		/// It is invoked when all the reels stop spinning and before Hit Check starts.
		/// </summary>
		public virtual void OnRoundInterval() { ShowDebugText("onRoundInterval"); }
		*/

        /// <summary>
        /// A callback method subscribed to CS's onProcessHit event.
        /// It is invoked when a Hit Check is performed on a line or a scatter and if it is successful.
        /// Information like which symbol holders were the subjects of the hit, how many chains the hit made and etc etc
        /// are passed with HitInfo.
        /// 
        /// HitInfo also has a reference to a DOTween sequence(<see cref="DG.Tweening.DOTween.Sequence"/> that plays highlighting effects for the hit symbols.
        /// 
        /// </summary>
        public virtual void OnProcessHit(HitInfo info)
        {
            int ScatterCount = 0;
            ShowDebugText("onProcessHit");
            if (info.hitSymbol.payType == Symbol.PayType.Normal)
            {
                //  assets.audioWinSmall.Play();
                RefreshMoney();
            }
            if (info.hitSymbol.payType == Symbol.PayType.Wild2x)
            {
                // assets.audioWinMedium.Play();
                RefreshMoney();
            }
            if (info.hitSymbol.payType == Symbol.PayType.Wild3x)
            {
                //  assets.audioWinBig.Play();
                RefreshMoney();
            }
            if (info.hitSymbol.payType == Symbol.PayType.FreesSpin)
            {
                assets.audioWinSpecial.Play();
                ScatterCount++;
            }
            Debug.Log("ScatterCount" + ScatterCount);
            // RefreshMoney();
            RefreshRoundCost();
            RefreshRoundInfo();


        }

        /// <summary>
        /// An UnityAction subscribed to CS's onRoundComplete event.
        /// It is invoked at the end of every round.
        /// </summary>
        public virtual void OnRoundComplete()
        {
            //if (CustomSlot.InstanceCust.dollarCounts == 1)
            //{
            //    ScatterHitsCount = CustomSlot.InstanceCust.dollarCounts;
            //}

            if (temp < 4)
            {
                PlayButton.interactable = false;
                InfoButton.interactable = false;
                BetDecrease.interactable = false;
                BetIncrease.interactable = false;
                TripleButtonsEnable(false);

            }
            else
            {

                PlayButton.interactable = true;
                InfoButton.interactable = true;
                BetDecrease.interactable = true;
                BetIncrease.interactable = true;
                TripleButtonsEnable(true);
                CheckFreeSpin();
            }



            // Debug.Log("FREE SPINS :: " + BaseSlotGameUI.Instance.AllFreeCounts);
            //if (CountScatter == 2 /*&& BaseSlotGameUI.Instance.AllFreeCounts != 0*/)
            //{

            //}

            //if (CountScatter == 3 /*&& BaseSlotGameUI.Instance.AllFreeCounts != 0*/)
            //{
            //    foreach (CustomSlot slot in slots)
            //    {
            //        slot.AddFreeSpin(8);
            //        Debug.Log("FREE SPINS :: " + BaseSlotGameUI.Instance.AllFreeCounts);
            //    }
            //}
            //if (CountScatter == 4 /*&& BaseSlotGameUI.Instance.AllFreeCounts != 0*/)
            //{
            //    foreach (CustomSlot slot in slots)
            //    {
            //        slot.AddFreeSpin(15);
            //        Debug.Log("FREE SPINS :: " + BaseSlotGameUI.Instance.AllFreeCounts);
            //    }
            //}

            Debug.Log("DollarCounts " + DollarCounts);
            ShowDebugText("onRoundComplete");

        }

        /// <summary>
        /// A callback method subscribed to CS's onSlotStateChange event.
        /// It is invoked when CS's state changes.
        /// </summary>
        public virtual void OnSlotStateChange()
        {
            ShowDebugText("onSlotStateChange");

        }

        /// <summary>
        /// A callback method subscribed to CS's onSlotModeChange event.
        /// It is invoked when CS's mode changes(e.g going to free spin mode).
        /// </summary>
        public virtual void OnSlotModeChange(SlotModeInfo info)
        {
            ShowDebugText("onSlotModeChange");
            foreach (CustomSlot slot in slots)
            {

                if (info.lastMode == slot.modes.freeSpinMode) ToggleFreeSpin(false);
                //if (info.lastMode == slot.modes.bonusMode) ToggleBonus(false);

                if (slot.currentMode == slot.modes.freeSpinMode) ToggleFreeSpin(true);
                {
                    // ShowBonusFreeSpin();
                }

                // if (slot.currentMode == slot.modes.bonusMode) ToggleBonus(true);
                // playScreens.SetActive(false);
            }

            RefreshFreeSpin();
            RefreshBonus();
        }
        void ShowBonusFreeSpin()
        {

            BonusBG.SetActive(true);
            BonusMessage.SetActive(true);
            BonusMessage.transform.DOScale(new Vector3(1f, 1f, 1f), 1f).SetEase(Ease.InOutBack).OnComplete(AllowToClose);

            StartButton.SetActive(true);
            for (int i = 0; i < FooterPanel.Length; i++)
            {
                if (FooterPanel[i].gameObject.activeSelf == true)
                {
                    FooterPanel[i].SetActive(false);

                }
            }

            if (BonusBG.activeInHierarchy || BonusAnimComplete)
            {
                for (int i = 0; i < FooterPanel.Length; i++)
                {
                    if (FooterPanel[i].gameObject.activeSelf == true)
                    {
                        FooterPanel[i].SetActive(false);
                    }
                }

                if (Input.GetMouseButtonDown(0))

                {
                    if (BonusAnimComplete)
                    {

                        BonusBG.SetActive(false);
                        BonusMessage.SetActive(false);
                        // SpinButton.GetComponent<Image>().sprite = SpinButtonSprite;
                        playScreens.SetActive(true);
                        foreach (GameObject gg in FooterPanel)
                        {
                            gg.SetActive(true);
                        }

                    }
                    else
                    {
                        return;
                    }

                }
            }
        }
        void AllowToClose()
        {
            BonusAnimComplete = true;
        }

        public void StartBonus()
        {
            if (BonusAnimComplete)
            {
                BonusBG.SetActive(false);

                playScreens.SetActive(true);
                StartButton.SetActive(false);
                foreach (GameObject gg in FooterPanel)
                {
                    gg.SetActive(true);
                }

                //foreach (CustomSlot slot in slots)
                //{
                //    if (slot.IsDollarFound)
                //    {
                //        slot.AddFreeSpin(8);
                //    }

                //}

            }
            if (TotalFreeSpin == 8)
            {
                AddFreeSpin(8);
            }
            else if (TotalFreeSpin == 15)
            {
                AddFreeSpin(15);
            }
            // SpinButton.GetComponent<Image>().sprite = SpinButtonSprite;

        }

        public void CheckFreeSpin()
        {
            if (DollarCounts == 3)
            {
                TotalFreeSpin = 8;
                ShowBonusFreeSpin();
            }
            if (DollarCounts == 4)
            {
                TotalFreeSpin = 15;
                ShowBonusFreeSpin();
            }

        }

        public void AddFreeSpin(int amount)
        {
            foreach (CustomSlot slot in slots)
            {
                slot.gameInfo.freeSpins += amount;
                if (slot.gameInfo.freeSpins < 0)
                    slot.gameInfo.freeSpins = 0;
            }

        }

        /// <summary>--
        /// An UnityAction subscribed to CS's onLineSwitch event.
        /// It is invoked every time a line is turned on/off.
        /// The current state of the line can be checked from the passed LineInfo. 
        /// </summary>
        public virtual void OnLineSwitch(LineInfo info)
        {
            ShowDebugText("onLineSwitch");
            RefreshRoundCost();
            RefreshRoundInfo();
        }

        public virtual void ShowDebugText(string detail)
        {
            if (!debugText) return;
            debugText.text = "Last Callback: " + detail;
        }

        public virtual void RefreshRoundCost()
        {

            roundCost = 0;
            //foreach (CustomSlot slot in slots)
            //    textRoundCost_.text = "" + (slot.gameInfo.roundCost*4);

            for (int i = 0; i < slots.Length; i++)
            {

                roundCost += slots[i].gameInfo.GetRounCost;

                textRoundCost_.text = "" + roundCost;

            }

        }
        public virtual void RefreshMoney()
        {
            //string TotalBal = "";
            //foreach (CustomSlot slot in slots)
            //{
            //    TotalBal = BaseSlotGameUI.Instance.Balance.ToString();
            //}

            //foreach (CustomSlot slot in slots)
            //{
            //    textMoney_.text = "" + slot.gameInfo.balance;
            //}
            textMoney_.text = BaseSlotGameUI.Instance.Balance.ToString();
        }
        public virtual void RefreshBet()

        {

            foreach (CustomSlot slot in slots)
            {
                textBet_.text = slot.gameInfo.bet.ToString();
            }

            // textBet_.text = "" + mybetCost;
            /*textBet.text = "" + slot.gameInfo.bet; */
        }

        public virtual void RefreshRoundInfo()
        {
            xx = 0;
            //textRound.text = "Round. " + (slot.gameInfo.roundsCompleted + 1);
            if (PlayButton.interactable == true)
                for (int i = 0; i < slots.Length; i++)
                {
                    xx += slots[i].PayoutAmount;

                    textIncome_.text = Convert.ToString(xx);
                }

            if (textIncome_.text == "0")
            {
                textIncome_.text = "";
            }
            RefreshFreeSpin();
            RefreshBonus();

        }

        public virtual void ToggleFreeSpin(bool enable) { goFreeSpin.SetActive(enable); }
        public virtual void RefreshFreeSpin()
        {
            //textFreeSpin.text = "" + slot.gameInfo.freeSpins;
            //if (DollarCounts == 2)
            //{
            //    textIncome_.text = Convert.ToString(xx * 1.5);
            //}
        }
        public virtual void ToggleBonus(bool enable)
        {
            goBonus.SetActive(enable);
        }
        public virtual void RefreshBonus()
        {
            //textBonus.text = "" + slot.gameInfo.bonuses;
        }

        public virtual bool SetBet(int index)
        {
            checkbalance();
            foreach (CustomSlot slot in slots)
            {
                if (!slot.isIdle || index < 0 || index >= betList.Count) return false;
                betIndex = index;
                slot.SetBet(betList[betIndex]);
            }

            RefreshBet();
            RefreshRoundCost();
            return true;
        }

        public virtual void RaiseBet()
        {
            checkbalance();
            foreach (CustomSlot slot in slots)
            {
                textIncome_.text = "";
                slot.StopPlaybackResult();

            }
            BetDecrease.interactable = true;
            if (betIndex < betList.Count)
            {
                SetBet(betIndex + 1);
            }
            if (betIndex == betList.Count - 1)
            {
                BetIncrease.interactable = false;

            }
        }

        public virtual void LowerBet()
        {
            checkbalance();
            foreach (CustomSlot slot in slots)
            {
                textIncome_.text = "";
                slot.StopPlaybackResult();
            }
            BetIncrease.interactable = true;
            if (betIndex > 0)
            {
                SetBet(betIndex - 1);
            }
            if (betIndex == 0)
            {
                BetDecrease.interactable = false;
            }
        }

        public virtual void EnableNextLine()
        {
            checkbalance();

            foreach (CustomSlot slot in slots)
            {
                LineAdd.interactable = true;

                if (slot.lineManager.activeLines == 25)
                {
                    LineAdd.interactable = false;

                }
                else
                {
                    LineRemove.interactable = true;
                    slot.lineManager.EnableNextLine();
                }
            }

        }
        public virtual void DisableCurrentLine()
        {
            checkbalance();
            foreach (CustomSlot slot in slots)
            {
                LineRemove.interactable = true;

                if (slot.lineManager.activeLines == 1)
                {
                    LineRemove.interactable = false;
                }
                else
                {
                    LineAdd.interactable = true;
                    slot.lineManager.DisableCurrentLine();
                }
            }

        }
        public void OnStartPlayClicked()
        {
            marketingtxt.text = marketingTxtChange[0];
            titleheader.SetActive(true);
            paybacklogo.SetActive(true);
            startscreen.SetActive(false);

            screenfooter.SetActive(false);
            Title_PlayTable.SetActive(false);
            Image_PlayTable.SetActive(false);
            Sound.interactable = true;
            coroutine = WaitAndPrint();
            StartCoroutine(coroutine);
        }
        IEnumerator WaitAndPrint()
        {
            for (int i = 0; i <= marketingTxtChange.Length; i++)
            {
                if (i == 0 || i == 4)
                {
                    MarketingIcon2.enabled = false;
                    MarketingIcon.enabled = false;
                    MarketingIcon3.enabled = false;
                }
                if (i == 1)
                {
                    MarketingIcon2.enabled = true;
                    MarketingIcon.enabled = false;
                    MarketingIcon3.enabled = false;
                }
                if (i == 2)
                {
                    MarketingIcon2.enabled = false;
                    MarketingIcon.enabled = true;

                    double GameCost = Convert.ToDouble(1.5 * Convert.ToInt32(textRoundCost_.text));
                    marketingTxtChange[i] = ("Landing on 2 Games              Pays" + " " + GameCost);
                }
                if (i == 3)
                {

                    MarketingIcon.enabled = false;
                    MarketingIcon3.enabled = true;
                }

                marketingtxt.text = marketingTxtChange[i];
                yield return new WaitForSeconds(MarketingTxtTime);

                if (i == 4)// For repeate marketing text loop
                {
                    i = -1;
                }
            }
        }

        public void ReturnToGame()
        {
            playtable.SetActive(false);
            screenfooter.SetActive(false);
            Title_PlayTable.SetActive(false);
            Image_PlayTable.SetActive(false);
            titleheader.SetActive(true);
            paybacklogo.SetActive(true);
            Sound.interactable = true;
        }
        public void PlaytableNextItem() // ADDED BY PRAGYA for show UI next page
        {
            for (int i = 0; i <= playtables.Length; i++)
            {

                if (playtables[i].gameObject.activeSelf == true)
                {
                    playtables[i + 1].SetActive(true);
                    playtables[i].SetActive(false);
                    i = playtables.Length;
                }

            }
        }

        public void ShowPlaytable(GameObject playtabl)
        {
            if (playtabl.activeSelf == false)
            {
                Sound.interactable = false;
                playtabl.SetActive(true);
                screenfooter.SetActive(true);
                navbar.SetActive(true);
            }


            else
                return;
        }
        public void PlaytablePreviousItem() // ADDED BY PRAGYA for show UI previous page
        {

            for (int i = 0; i <= playtables.Length; i++)
            {

                if (playtables[i].gameObject.activeSelf == true)
                {
                    playtables[i - 1].SetActive(true);
                    playtables[i].SetActive(false);
                    i = playtables.Length;
                }

            }

        }
        public void Update() // ADDED BY PRAGYA for show/hide UI buttons
        {


            if (panel4.activeSelf == true)
            {
                NextBtn.gameObject.SetActive(false);
            }
            else
            {
                NextBtn.gameObject.SetActive(true);
            }
            if (panel1.activeSelf == true)
            {
                NextBtn.gameObject.SetActive(true);
                BackBtn.gameObject.SetActive(false);
            }
            else
            {
                BackBtn.gameObject.SetActive(true);
            }

            if (playtable.activeSelf)
            {
                Title_PlayTable.SetActive(true);
                Image_PlayTable.SetActive(true);

                titleheader.SetActive(false);
                paybacklogo.SetActive(false);
                marketingText.gameObject.SetActive(false);
            }
            else
            {

                marketingText.gameObject.SetActive(true);


            }



            if (screenfooter.activeSelf)
            {
                Title_PlayTable.SetActive(true);
                Image_PlayTable.SetActive(true);
                screenheader.GetComponent<Image>().enabled = true;

            }
            else
            {
                Title_PlayTable.SetActive(false);
                Image_PlayTable.SetActive(false);
                screenheader.GetComponent<Image>().enabled = false;

            }


            SetTripleSlot();

            //if (startscreen)
            //{
            //    if (Input.GetKeyDown(KeyCode.Space))
            //    {
            //        OnStartPlayClicked();
            //    }
            //}
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (startscreen.gameObject.activeInHierarchy == true)
                {
                    OnStartPlayClicked();
                }
                else
                {
                    if (BonusBG.activeInHierarchy == false)
                    {
                        if (PlayButton.interactable == true)
                            PlayAllSlots();
                    }


                }
            }

            if (PlayButton.interactable == false)
            {
                textIncome_.text = "";
            }

            for (int i = 0; i < slots.Length; i++)   // for checking which slot is tripled
            {
                FreeSpin[i] = slots[i].gameInfo.freeSpins;

            }
            compare = Convert.ToInt16(BaseSlotGameUI.Instance.textRoundCost_.text);
            checkbalance();
        }

        public void checkbalance()
        {

            // compare = Convert.ToInt16(BaseSlotGameUI.Instance.textRoundCost_.text);

            if (Balance < compare)
            {
                BaseSlotGameUI.Instance.PlayButton.interactable = false;
                if (BaseSlotGameUI.Instance.Balance < 0 || BaseSlotGameUI.Instance.Balance == 0)
                {
                    BaseSlotGameUI.Instance.PlayButton.interactable = false;
                }
            }
            else if (Balance == compare && InfoButton.interactable == true)
            {
                PlayButton.interactable = true;
            }
            else if (Balance > compare && InfoButton.interactable == true)
            {
                PlayButton.interactable = true;
            }
        }


        public void SelectTripleSlot(CustomSlot slot)
        {

            if (Input.GetKeyDown(KeyCode.Space) == false)
            {


                if (slot.IsTripleSlot)

                {
                    slot.IsTripleSlot = false;
                    // SelectTripleSlotRandomly();
                    TripleBet(slot); // checking if triplled 
                    checkbalance();
                }
                else
                {
                    //foreach (CustomSlot slott in slots)
                    //{

                    //    var bgcolors = slott.GetComponent<Image>();
                    //    TextMeshProUGUI bettexts = slott.tripleBetButton.GetComponentInChildren<TextMeshProUGUI>();
                    //    slott.IsTripleSlot = false;


                    //    //bettexts.text = "TRIPLE THIS BET";
                    //    //bgcolors.color = Color.white;
                    //    //if (!slot.IsTripleSlot)
                    //    //{

                    //    //}
                    //    bettexts.text = "TRIPLE THIS BET";
                    //    bgcolors.color = Color.white;
                    //    //if (!slot.IsTripleSlot)
                    //    //{

                    //    //}

                    //    TripleBETVal(slott);

                    //}
                    for (int i = 0; i < slots.Length; i++)
                    {


                        slots[i].IsTripleSlot = false;
                        TripleBet(slots[i]);
                        //var bgcolors = slots[i].GetComponent<Image>();
                        //TextMeshProUGUI bettexts = slots[i].tripleBetButton.GetComponentInChildren<TextMeshProUGUI>();
                        //bettexts.text = "TRIPLE THIS BET";
                        //bgcolors.color = Color.white;
                        //slots[i].tripleBetButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 170);
                        //slots[i].tripleBetText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 100);
                    }

                    slot.IsTripleSlot = true;

                    TripleBet(slot);
                    checkbalance();
                }

            }

        }
        public void TripleBETVal(CustomSlot slot)
        {
            slot.tripleBetButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 170);
            slot.tripleBetText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 100);
            slot.tripleBetText.font = Resources.Load<TMP_FontAsset>("PoetsenOne-Regular SDF(TMP_Font Assets)");
            slot.tripleBetText.color = Color.black;
            slot.tripleBetText.fontStyle = FontStyles.Bold;
            slot.tripleBetText.fontSize = 25f;
        }
        public void TripleBet(CustomSlot slot)
        {


            var bgcolor = slot.GetComponent<Image>();
            TextMeshProUGUI bettext = slot.tripleBetButton.GetComponentInChildren<TextMeshProUGUI>();


            if (!slot.IsTripleSlot)
            {

                bettext.text = "TRIPLE THIS BET";
                bgcolor.color = Color.white;
                TripleBETVal(slot);
            }
            else
            {

                bettext.text = "RESET";
                bgcolor.color = Color.yellow;
            }
            RefreshBet();
            RefreshMoney();
            RefreshRoundCost();
        }


        public void SelectTripleSlotRandomly()
        {
            int randomIndex;
            randomIndex = UnityEngine.Random.Range(0, slots.Length);
            SelectTripleSlot(slots[randomIndex]);

        }

        public void SetTripleSlot()
        {

            for (int i = 0; i < slots.Length; i++)   // for checking which slot is tripled
            {
                if (slots[i].IsTripleSlot)
                {
                    isRichSlot[i] = true;
                    slots[i].tripleBetText.text = "BET TRIPLED";

                    slots[i].tripleBetButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 80);
                    slots[i].tripleBetText.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 120);

                    slots[i].tripleBetText.font = Resources.Load<TMP_FontAsset>("PoetsenOne-Regular SDF(TMP_Font Asset)");

                    slots[i].tripleBetText.color = Color.red;
                    slots[i].tripleBetText.fontStyle = FontStyles.Bold;

                    slots[i].tripleBetText.fontSize = 25f;

                }
                else if (!slots[i].IsTripleSlot)
                {
                    isRichSlot[i] = false;
                    slots[i].tripleBetText.text = "BET";

                }
                checkbalance();
            }


        }
        public void MuteSound()
        {
            if (AudioListener.volume != 0f)
            {
                AudioListener.volume = 0f;
                Sound.GetComponent<Image>().sprite = mute;
            }
            else
            {
                AudioListener.volume = 2f;
                Sound.GetComponent<Image>().sprite = unmute;
            }
        }
    }
}
